[![pipeline status](https://gitlab.com/homelabgroup/cicd/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/homelabgroup/cicd/-/commits/main)

# Templates
This project holds non-repository-specific CI/CD files (templates) used for various tasks like pulling, building and tagging images, and running mainenance jobs.
